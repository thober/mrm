# mRM RELEASE NOTES

## mRM v1.0.1 (Sep 2019)

### New Features:

- None

### Bugs resolved from release 1.0:

- corrected parameter names in mrm_parameter.nml

### Known bugs:

None.

### Restrictions:

- For `gfortran` compilers mHM supports only v4.8 and higher.

## mRM v1.0 (May 2019)

### New Features:

- Implemented adaptive timestep routing as described in (https://www.geosci-model-dev.net/12/2501/2019/)

### Known bugs:

None.

### Restrictions:

- For `gfortran` compilers mHM supports only v4.8 and higher.
