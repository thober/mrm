# The multiscale Routing Model -- mRM

- The current release is **[mRM v1.0.1][1]**.
- The latest mRM release notes can be found in the file [RELEASES][3] or [online][4].
- General information can be found on the [mHM website](http://www.ufz.de/mhm/).
- All the details of the code can be found in the [user manual][5].
- The mRM comes with a [LICENSE][6] agreement, this includes also the GNU Lesser General Public License.

**Please note:** The [GitLab repository](https://git.ufz.de/mhm/mrm) grants read access to the code.
If you like to contribute to the code, please contact [mhm-admin@ufz.de](mailto:mhm-admin@ufz.de).

## Cite as

Please refer to the main model by citing Thober et al. (2019),  Kumar
et al. (2013), and Samaniego et al. (2010):

- Thober, S., Cuntz, M., Kelbling, M., Kumar, R., Mai, J., and Samaniego, L.: The multiscale Routing Model mRM v1.0: simple river routing at resolutions from 1 to 50 km, Geosci. Model Dev. Discuss., https://doi.org/10.5194/gmd-2019-13, in review, 2019
- Kumar, R., L. Samaniego, and S. Attinger (2013): Implications of distributed hydrologic model parameterization on water fluxes at multiple scales and locations, Water Resour. Res., 49, doi:10.1029/2012WR012195, http://onlinelibrary.wiley.com/doi/10.1029/2012WR012195/abstract
- Samaniego L., R. Kumar, S. Attinger (2010): Multiscale parameter regionalization of a grid-based hydrologic model at the mesoscale. Water Resour. Res., 46,W05523, doi:10.1029/2008WR007327, http://onlinelibrary.wiley.com/doi/10.1029/2008WR007327/abstract

The model code can be cited as:

Thober et al. (2019), multiscale Routing Model mRM, doi: 10.5281/zenodo.3229679

## Install

Please see the file [DEPENDENCIES][7] for external software required to run mRM and the [INSTALL][8] for using cmake to compile mRM.
See also the [users manual][5] for detailed instructions to setup mRM.


## Quick start

1. Follow the cmake guide in [INSTALL][8] to compile mRM. Alternatively (not recommended), compile mRM with the `make` command, which uses settings from [Makefile](Makefile). This is only available for specific systems.
2. Run mRM on a test basin with the command `./mrm`, which uses settings from [mrm.nml](mrm.nml).
3. Explore the results in the [output directory](test_basin/), e.g. by using the NetCDF viewer `ncview`.


[1]: https://git.ufz.de/mhm/mrm/tags/1.0
[2]: https://git.ufz.de/mhm/mrm/repository/1.0/archive.zip
[3]: doc/RELEASES.md
[4]: https://git.ufz.de/mhm/mrm/tags/
[5]: doc/mhm_manual_v5.9.pdf
[6]: LICENSE
[7]: doc/DEPENDENCIES.md
[8]: INSTALL.md
